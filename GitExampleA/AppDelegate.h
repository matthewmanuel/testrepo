//
//  AppDelegate.h
//  GitExampleA
//
//  Created by Matthew Manuel on 17/08/2014.
//  Copyright (c) 2014 Matthew Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
