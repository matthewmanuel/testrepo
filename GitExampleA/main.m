//
//  main.m
//  GitExampleA
//
//  Created by Matthew Manuel on 17/08/2014.
//  Copyright (c) 2014 Matthew Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
